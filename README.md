# [Устарел] Android Studio Template - Error Class

Шаблон для создания ошибки

Создает следующие классы:
  - `enum class ${name}Code`
  - `class ${name}`

Зависит от [core-mvp](https://gitlab.com/hikkidev/android-modules/tree/master/core-mvp) модуля и добавлет в зависимости (если его нет)
# Установка

1. Скопировать папку шаблона в `{path to Android Studio}\plugins\android\lib\templates\other`
2. Перезапустить студию (если она была открыта)

# Использование

Right Click or File -> New -> Custom -> Error Class  
`Ctrl + Shift + A` -> Error Class
