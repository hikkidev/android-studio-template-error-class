<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>
        <@kt.addAllKotlinDependencies />
        <#if !(hasDependency('com.hikkideveloper:core-mvp'))>
                <dependency mavenUrl="com.hikkideveloper:core-mvp:1.0.2"/>
        </#if>

        <#if generateKotlin>
        <instantiate from="src/app_package/Class.kt.ftl"
                to="${escapeXmlAttribute(srcOut)}/${name}.kt" />
        <open file="${escapeXmlAttribute(srcOut)}/${name}.kt" />
        </#if>
</recipe>