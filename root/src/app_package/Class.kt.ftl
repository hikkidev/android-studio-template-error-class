package ${escapeKotlinIdentifiers(packageName)}

import com.hikkidev.base.error.BaseError
import com.hikkidev.base.error.BaseErrorCode

enum class ${name}Code : BaseErrorCode {
    Unspecified { override val rawCode = 0 },
}

class ${name}(code: ${name}Code, cause: Throwable? = null, msg: String? = null) : BaseError(code, cause, msg) {
    override val domainShortName = TODO("Придумать двух-буквенный идентификатор ошибки.") 
}
